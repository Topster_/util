import os
import getpass
import requests

if os.geteuid() != 0:
	exit("Você precisa ser root pra fazer isso. Rode: sudo python3 ts3_install.py")
r = requests.get("https://files.teamspeak-services.com/releases/client/3.5.6/TeamSpeak3-Client-linux_amd64-3.5.6.run",stream=True)
size = int(r.headers.get("content-length"))
with open("TeamSpeak3-Client-linux_amd64-3.5.6.run","wb") as f:
        done = 0
        for chunk in r.iter_content(chunk_size=65536):
                done += len(chunk)
                print("Baixando: {}%".format(int((done/size)*100)),end="\r")
                f.write(chunk)
print("\nRodando .run...")
os.system("chmod +x TeamSpeak3-Client-linux_amd64-3.5.6.run")
os.system("./TeamSpeak3-Client-linux_amd64-3.5.6.run")
print("Movendo a pasta...")
os.system("mv TeamSpeak3-Client-linux_amd64 /opt/")
print("Baixando icone...")
r = requests.get("https://cdn3.iconfinder.com/data/icons/logos-and-brands-adobe/512/333_Teamspeak-512.png")
with open("/opt/TeamSpeak3-Client-linux_amd64/ts3_icon.png","wb") as f:
	f.write(r.content)
	f.close()
print("Criando .desktop...")
with open("/usr/share/applications/teamspeak3.desktop","w") as f:
	f.write("""[Desktop Entry]
Name=TeamSpeak 3
Comment=TeamSpeak 3 VoIP
Exec=/opt/TeamSpeak3-Client-linux_amd64/ts3client_runscript.sh
Terminal=false
Type=Application
Categories=Network;Application;
Icon=/opt/TeamSpeak3-Client-linux_amd64/ts3_icon.png""")
print("Apagando instalador...")
os.system("rm -rf TeamSpeak3-Client-linux_amd64-3.5.6.run")
print("\nInstalação completada com sucesso !")
